const simianService = require('../services/simian');

module.exports.isSimian = (req, res)=> {

	try {

		if (!req.body || !req.body.dna)
			return res.status(403).json({ error: "Cannot find dna in request body" })

			
		if (!Array.isArray(req.body.dna))
			return res.status(403).json({ error: "Dna is not an Array" })
			

		let isSimian = simianService.isSimian(req.body.dna);

		if (isSimian)
			res.status(200).json({ isSimian })
		else
			res.status(403).json({ isSimian })

	} catch (error) {
		console.log(error)
		res.status(500).json({ error: error.message })
	}

}

module.exports.stats = (req, res) => {

	simianService.stats().then((stats)=>{
		res.status(200).json(stats)
	})
	.catch((error)=>{
		console.log(error)
		res.status(500).json({ error: error.message })
	})

}