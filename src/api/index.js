const {version} =  require('../../package.json');
const {Router} = require('express');
const simianRouter = require('./simian');


module.exports = () => {
	let api = Router();

	api.post('/simian', simianRouter.isSimian);
	api.get('/stats', simianRouter.stats);

	api.get('/', (req,res) => {
		res.json({ version });
	});

	return api;
}
