const http = require('http');
const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const middleware = require('./middleware');
const api = require('./api');
const config = require('./config.json');
const mongoose = require('mongoose');

let app = express();
app.server = http.createServer(app);

app.use(morgan('dev'));

app.use(cors({
	exposedHeaders: config.corsHeaders
}));

app.use(bodyParser.json({
	limit: config.bodyLimit
}));

mongoose.connect(process.env.MONGO_HOST, { useNewUrlParser: true }).then(() => {

	app.use(middleware({ config }));
	app.use('/', api());

	if (process.env.NODE_ENV !== 'test')
		app.server.listen(process.env.PORT || config.port, () => {
			console.log(`Started on port ${app.server.address().port}`);
		});
}).catch((error) => {
	console.log('db connection failed', error);
});

module.exports.app = app;
