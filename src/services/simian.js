const dnRepository = require('../repositories/dna');

class SimianService {


    isSimian(dna) {

        if (!this.checkValidCharacters(dna))// Verificando se tem charcteres diferentes de TGCA
            return false

        if (dna.join('').length < 16)// a Matriz minima é 4 x4 
            return false

        if (!this.checkDimensionIsValid(dna)) // Dimensao invalida
            return false


        if (this.checkSimianInHorizontal(dna)) // Processamento horizontal
            return this.buildResult(dna, true)

        if (this.checkSimianInVertical(dna)) // Processamento vertical
            return this.buildResult(dna, true)

        if (this.checkSimianInDiagonal(dna))  // Processamento diagonal
            return this.buildResult(dna, true)

        return this.buildResult(dna, false)

    }

    buildResult(dna, isSimian) {
        dnRepository.save(dna, isSimian).catch((error)=>{
            console.log('Error on save analyse', error.message)
        });

        return isSimian
    }

    checkValidCharacters(dna) {

        let stringToCheck = dna.join('');


        if (stringToCheck.match(/^[TGCA>]*$/) === null)
            return false

        return true;
    }

    checkDimensionIsValid(dna) {

        let dimension = dna[0].length;
        let invalidDimension = false;

        dna.forEach(element => {
            if (element.length != dimension)
                invalidDimension = true
        });

        return !invalidDimension
    }

    checkSimianInHorizontal(dna) {
        let dnaString = dna.join('-')

        return this.checkOccorrencies(dnaString);
    }

    checkSimianInVertical(dna) {

        let dnaInverse = dna.map((col, i) => dna.map(row => row[i]));

        let dnaStringInverse = dnaInverse.join("-").replace(/,/g, '')

        return this.checkOccorrencies(dnaStringInverse);
    }

    checkSimianInDiagonal(dna) {

        let dnaStringBottomToTop = this.diagonal(dna, true).join('-');
        let dnaStringTopToRight = this.diagonal(dna, false).join('-');

        return this.checkOccorrencies(dnaStringBottomToTop) || this.checkOccorrencies(dnaStringTopToRight);
    }

    checkOccorrencies(dnaString) {
        if (dnaString.includes('AAAA') || dnaString.includes('TTTT') || dnaString.includes('CCCC') || dnaString.includes('GGGG'))
            return true

        return false;
    }

    diagonal(array, bottomToTop) {
        let Ylength = array.length;
        let Xlength = array[0].length;
        let maxLength = Math.max(Xlength, Ylength);
        let temp;
        let returnArray = [];
        for (let k = 0; k <= 2 * (maxLength - 1); ++k) {
            temp = [];
            for (let y = Ylength - 1; y >= 0; --y) {
                let x = k - (bottomToTop ? Ylength - y : y);
                if (x >= 0 && x < Xlength) {
                    temp.push(array[y][x]);
                }
            }
            if (temp.length > 0) {
                returnArray.push(temp.join(''));
            }
        }
        return returnArray;
    }

    async stats(){
        return dnRepository.stats();
    }

}

module.exports = new SimianService();
