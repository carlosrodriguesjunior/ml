const { Dna } = require('../models/dna');

class DnaRepository {

    async save(geneticCode, isSimian) {

        if (!geneticCode)
            throw ('parameter geneticCode is required')

        if (!Array.isArray(geneticCode))
            throw ('parameter geneticCode must be an array')

        if (typeof isSimian !== 'boolean')
            throw ('parameter isSimian is required')


        let dna = await Dna.create({
            geneticCode,
            geneticCodeKey: geneticCode.join(),
            isSimian
        });

        return dna;
    }

    async stats() {
        let stats = await Dna.aggregate(
            [
                {
                    $group: {
                        _id: 1,
                        count_mutant_dna: { $sum: { $cond: ["$isSimian", 1, 0] } },
                        count_human_dna: { $sum: { $cond: ["$isSimian", 0, 1] } }
                    }
                },
                { $project: { _id: 0 } }
            ]
        )

        let ratio = stats[0].count_mutant_dna / (stats[0].count_mutant_dna + stats[0].count_human_dna);

        return {
            count_mutant_dna: stats[0].count_mutant_dna,
            count_human_dna: stats[0].count_human_dna,
            ratio: parseFloat(ratio.toFixed(2))

        };

    }

}

module.exports = new DnaRepository();
