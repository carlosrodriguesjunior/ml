const mongoose = require('mongoose');
const { Schema } = mongoose;

const DnaSchema = new Schema({
  geneticCode: {
    type: Array
  },
  geneticCodeKey: {
    type: String,
    unique: true
  },
  isSimian: {
    type: Boolean
  },
  createdAt: {
    type: Date,
    default: Date.now,
  }

});

module.exports.DnaSchema = DnaSchema;
module.exports.Dna = mongoose.model('Dna', DnaSchema);
