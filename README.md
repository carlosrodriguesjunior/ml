ML - API Simian
==================================

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/fbcc8a3b39574d53939e29e47f5edd21)](https://www.codacy.com?utm_source=bitbucket.org&amp;utm_medium=referral&amp;utm_content=carlosrodriguesjunior/ml&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/fbcc8a3b39574d53939e29e47f5edd21)](https://www.codacy.com?utm_source=bitbucket.org&utm_medium=referral&utm_content=carlosrodriguesjunior/ml&utm_campaign=Badge_Coverage)

- instalar node 11.x (recomendo fortemente o uso de nvm - https://github.com/nvm-sh/nvm)
- npm i nodemon mocha nyc -g  - (instala dependências globais)
- npm i  - (instala dependências do projeto)
- npm run dev  - (Sobe a API em localhost e monitora alterações de arquivos para resetar a API por padrão usa a porta 8080, mas setar uma variável de ambiente "PORT")

- npm start - (Sobe a API em localhost por padrão usa a porta 8080, mas setar uma variável de ambiente "PORT")

- npm t - (executa os testes unitários, é preciso setar a variavel de ambiente NODE_ENV=test)
- npm run check-coverage - (verifica se os testes unitário estão com uma cobertura de pelo menos 80%, é nescessário executar os testes antes.)
- npm run lint - (verifica as boas práticas de programação através do eslint, js 2017)

- Conexão com o banco de dados, é esperado que a variavel de ambiente MONGO_HOST tenha a conexão do MongoDB, recomendado versão 4.x

Teste API - CURL
---------------
- "/" - health check API
curl -X GET \
  http://awseb-AWSEB-1QZVK7V8TDPSM-371788374.sa-east-1.elb.amazonaws.com \
  -H 'Accept: */*' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Host: awseb-AWSEB-1QZVK7V8TDPSM-371788374.sa-east-1.elb.amazonaws.com' \
  -H 'Postman-Token: 8ecf6b62-0523-4081-ba07-a72af34f1226,4e37750c-8435-474c-beb0-77359289c169' \
  -H 'User-Agent: PostmanRuntime/7.11.0' \
  -H 'accept-encoding: gzip, deflate' \
  -H 'cache-control: no-cache'

- "/simian" - health check API
curl -X POST \
  http://awseb-AWSEB-1QZVK7V8TDPSM-371788374.sa-east-1.elb.amazonaws.com/simian \
  -H 'Accept: */*' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Type: application/json' \
  -H 'Host: awseb-AWSEB-1QZVK7V8TDPSM-371788374.sa-east-1.elb.amazonaws.com' \
  -H 'Postman-Token: a6b26a79-e8ae-4f84-8964-ad555ab9913b,c93e4466-5110-45d3-bda3-fdd5c83a45ec' \
  -H 'User-Agent: PostmanRuntime/7.11.0' \
  -H 'accept-encoding: gzip, deflate' \
  -H 'cache-control: no-cache' \
  -H 'content-length: 72' \
  -d '{
	"dna": ["CTGAGA", "CTATGC", "TATTGT", "AGAGGG", "CCCGTA", "TCACTG"]
}'

- "/simian" - health check API
curl -X POST \
  http://awseb-AWSEB-1QZVK7V8TDPSM-371788374.sa-east-1.elb.amazonaws.com/simian \
  -H 'Accept: */*' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Type: application/json' \
  -H 'Host: awseb-AWSEB-1QZVK7V8TDPSM-371788374.sa-east-1.elb.amazonaws.com' \
  -H 'Postman-Token: a6b26a79-e8ae-4f84-8964-ad555ab9913b,c93e4466-5110-45d3-bda3-fdd5c83a45ec' \
  -H 'User-Agent: PostmanRuntime/7.11.0' \
  -H 'accept-encoding: gzip, deflate' \
  -H 'cache-control: no-cache' \
  -H 'content-length: 72' \
  -d '{
	"dna": ["CTGAGA", "CTATGC", "TATTGT", "AGAGGG", "CCCGTA", "TCACTG"]
}'

- "/stats" - health check API
curl -X POST \
  http://awseb-AWSEB-1QZVK7V8TDPSM-371788374.sa-east-1.elb.amazonaws.com/simian \
  -H 'Accept: */*' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Type: application/json' \
  -H 'Host: awseb-AWSEB-1QZVK7V8TDPSM-371788374.sa-east-1.elb.amazonaws.com' \
  -H 'Postman-Token: a6b26a79-e8ae-4f84-8964-ad555ab9913b,c93e4466-5110-45d3-bda3-fdd5c83a45ec' \
  -H 'User-Agent: PostmanRuntime/7.11.0' \
  -H 'accept-encoding: gzip, deflate' \
  -H 'cache-control: no-cache' \
  -H 'content-length: 72' \
  -d '{
	"dna": ["CTGAGA", "CTATGC", "TATTGT", "AGAGGG", "CCCGTA", "TCACTG"]
}'

Postman Doc
---------------
- Postman Doc - https://documenter.getpostman.com/view/122682/S1LpbC9y
