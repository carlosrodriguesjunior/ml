const chai = require('chai');
const expect = require('chai').expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai')
const simianService = require('../../src/services/simian');
const dnaRepository = require('../../src/repositories/dna');

chai.use(sinonChai);

const invalidCharacters = ["ATFGA", "ATGA", "ATGA", "ATG"];
const smallerMatrix = ["ATGA", "ATGA", "ATGA", "ATG"];
const invalidDimension = ["ATGA", "ATGA", "ATGA", "ATGG", "ATG"];
const humanDna = ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"];
const simianDnaHorizontal = ["CTGCGA", "CTATGC", "TATTAT", "AGAGGG", "CCCCTA", "TCACTG"];
const simianDnaVertical = ["CTGAGA", "CTATGC", "TATTGT", "AGAGGG", "CCCGTA", "TCACTG"];
const simianDnaDiagonal = ["CTGAGA", "CTATAC", "TATTGT", "AGAGCG", "CCCGTA", "TCACTG"];


// before(() => {
//     sinon.stub(dnaRepository, 'save').rejects('Error when save duplicate dna')
//   });

describe('Simian Service test', ()=>{
    it('Should be return false whith invalid Characters', (done)=>{
        expect(simianService.isSimian(invalidCharacters)).to.be.false;
        done()
    })

    it('Should be return false whith matrix smaller than 4 x4', (done)=>{
        expect(simianService.isSimian(smallerMatrix)).to.be.false;
        done()
    })

    it('Should be return false whith matrix has a invalid dimension', (done)=>{
        expect(simianService.isSimian(invalidDimension)).to.be.false;
        done()
    })

    it('Should be return false whith human dna', (done)=>{
        expect(simianService.isSimian(humanDna)).to.be.false;
        done()
    })

    it('Should be return true whith simian in Horizontal', (done)=>{
        expect(simianService.isSimian(simianDnaHorizontal)).to.be.true;
        done()
    })

    it('Should be return true whith simian in Vertical', (done)=>{
        expect(simianService.isSimian(simianDnaVertical)).to.be.true;
        done()
    })

    it('Should be return true whith simian in Diagonal', (done)=>{
        expect(simianService.isSimian(simianDnaDiagonal)).to.be.true;
        done()
    })

    it('Validate stats is called', (done)=>{
        sinon.stub(dnaRepository, 'stats').returns({
            "count_mutant_dna": 3,
            "count_human_dna": 1,
            "ratio": 0.75
        });
        simianService.stats().then((stats)=>{
            expect(dnaRepository.stats).to.be.called;
            expect(stats).to.have.property('count_mutant_dna');
            expect(stats).to.have.property('count_human_dna');
            expect(stats).to.have.property('ratio');
            dnaRepository.stats.restore();
            done()
        });
    })
})

// after(() => {
//     dnaRepository.save.restore();
//   });