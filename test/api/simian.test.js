const chai = require('chai');
const expect = require('chai').expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai')
const simianApi= require('../../src/api/simian');
const simianService = require('../../src/services/simian');

chai.use(sinonChai);

let req, res;

describe('Simian Api test', ()=>{

    beforeEach(function() {
        req = {
            body: {},
        };

        res = {
            statusCalledWith: null,
            jsonCalledWith: null,
            json: function(arg) { 
                this.jsonCalledWith = arg;
            },
            status: function(arg) { 
                this.statusCalledWith = arg;
                return this;
            }
        };
    });

    describe('isSimian method', ()=>{
        it('Should be return error `Cannot find dna in request body`', (done)=>{
            simianApi.isSimian(req,res);
            expect(res.statusCalledWith).to.be.equals(403);
            expect(res.jsonCalledWith.error).to.be.equals('Cannot find dna in request body');
            done()

        })

        it('Should be return error `Dna is not an Array`', (done)=>{
            req.body = {dna:'a'};
            simianApi.isSimian(req,res);
            expect(res.statusCalledWith).to.be.equals(403);
            expect(res.jsonCalledWith.error).to.be.equals('Dna is not an Array');
            req.body = {};
            done()
        })

        it('Should be return false for a humanDna', (done)=>{
            req.body = {dna:["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"]};
            simianApi.isSimian(req,res);
            expect(res.statusCalledWith).to.be.equals(403);
            expect(res.jsonCalledWith.isSimian).to.be.equals(false);
            req.body = {}
            done()
        })

        it('Should be return true for a simianDna', (done)=>{
            req.body = {dna:["CTGCGA", "CTATGC", "TATTAT", "AGAGGG", "CCCCTA", "TCACTG"]};
            simianApi.isSimian(req,res);
            expect(res.statusCalledWith).to.be.equals(200);
            expect(res.jsonCalledWith.isSimian).to.be.equals(true);
            req.body = {}
            done()
        })

    })

    describe('stats method', ()=>{

        it('Should be return stats', (done)=>{
            let result = {
                "count_mutant_dna": 3,
                "count_human_dna": 1,
                "ratio": 0.75
            };
            let simianSpy =sinon.stub(simianService, 'stats').resolves(result);
            simianApi.stats(req, res);
            expect(simianSpy.calledOnce).to.be.true; 
            simianService.stats.restore();
            done()
        })


    })

})
