const chai = require('chai');
const expect = require('chai').expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai')

const mongoose = require('mongoose');


chai.use(sinonChai);

describe('Index Api test', ()=>{

    it('api',()=>{
        sinon.stub(mongoose, 'connect').resolves({connected:true})
        const index = require('../src/index');

        expect(mongoose.connect).to.be.called;
        mongoose.connect.restore();
        
    })



});

