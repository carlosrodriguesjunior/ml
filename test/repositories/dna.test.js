const chai = require('chai');
const expect = require('chai').expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai')
const dnaRepository = require('../../src/repositories/dna');
const { Dna } = require('../../src/models/dna');

chai.use(sinonChai);

describe('Dna Repository test', ()=>{

    describe('Save method',()=>{
        it('Save dna should be failed if dont send a parameter geneticCode ', (done)=>{

            dnaRepository.save(null,true)
            .catch((error)=>{
                expect(error).be.equals('parameter geneticCode is required');
                done()
            });
            
        })
    
        it('Save dna should be failed if parameter geneticCode is not an array', (done)=>{
    
            dnaRepository.save('a',true)
            .catch((error)=>{
                expect(error).be.equals('parameter geneticCode must be an array');
                done()
            });
            
        })
    
        it('Save dna should be failed if dont send a parameter isSimian ', (done)=>{
            dnaRepository.save([],null)
            .catch((error)=>{
                expect(error).be.equals('parameter isSimian is required');
                done()
            });
            
        })
    
        it('Save dna should be ok', (done)=>{
            sinon.stub(Dna, 'create').returns({"_id":"5cc4aa102e706425ea5eda6e","geneticCode":["CTGCGA","CTATGC","TATTAT","AGAGGG","CCCCTA","TCACTG"],"geneticCodeKey":"CTGCGA,CTATGC,TATTAT,AGAGGG,CCCCTA,TCACTG","isSimian":true,"createdAt":"2019-04-27T19:14:24.675Z","__v":0});
            dnaRepository.save([],true).then((dna)=>{
                expect(Dna.create).to.be.called;
                Dna.create.restore()
                done()
            })
            
        })
    })

    describe('Stats method',()=>{
        it('get stats should be ok', (done)=>{
            sinon.stub(Dna, 'aggregate').returns([{
                count_mutant_dna: 3,
                count_human_dna: 1,
                ratio: 0.75
     
            }]);
            dnaRepository.stats().then((stats)=>{
                expect(Dna.aggregate).to.be.called;
                expect(stats).to.have.property('count_mutant_dna');
                expect(stats).to.have.property('count_human_dna');
                expect(stats).to.have.property('ratio');
                Dna.aggregate.restore();
                done()
            });
            
        });

    });

});